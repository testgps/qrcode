import { StatusBar } from "expo-status-bar";
import { useContext, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import PanelQRCode from "./components/PanelQRCode";
import { QRCodeProvider } from "./providers/QRCodeProvider";
import QRCode from "./components/QRCode";

export default function App() {
  return (
    <View style={styles.container}>
      <QRCodeProvider>
        <PanelQRCode />
        <QRCode> </QRCode>
        <StatusBar style="auto" />
      </QRCodeProvider>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
