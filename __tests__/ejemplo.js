function suma(a, b) {
  return a + b;
}

test('SUMA DE 4 + 5 ES IGUAL A 9', () => {
  expect(suma(4, 5)).toBe(9);
});
