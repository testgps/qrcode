import { Crypto } from '../utils/Crypto';

test('SE EJECUTÓ ', async () => {
  const data = '2020371001@uteq.edu.mx';
  const crypto = Crypto('AES');
  const encriptar = await crypto.encriptar(data);
  console.log(encriptar);
  console.log(data);
  const hola = await crypto.desencriptar(encriptar);
  expect(data).toBe(hola);
});
