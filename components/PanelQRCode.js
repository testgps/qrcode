import { TextInput } from 'react-native';
import { useContextPanelQRCode } from '../providers/QRCodeProvider';
import { View } from 'react-native-web';
import NativeColorPicker from 'native-color-picker';
import Slider from '@react-native-community/slider';
import { useRef, useState } from 'react';
import * as Crypto from 'expo-crypto';
import { Dropdown } from 'react-native-element-dropdown';
import { ColorPicker } from 'react-native-color-picker';
import { tipoAlgoritmoCripto } from '../utils/Crypto';

export default function PanelQRCode() {
  const [selected, setSelected] = useState('#db643a');
  const [state, dispatch] = useContextPanelQRCode();
  const [typeCrypto, setTypeCrypto] = useState('MD5');
  const [text, setText] = useState('Hola');
  const colorPickerRef = useRef(null);

  const onDataChange = (value) => {
    dispatch({ type: 'UPDATE_TEXT', text: value });
    setText(value);
  };

  const onColorChange = (color) => {
    setSelected(color);
    dispatch({ type: 'UPDATE_COLOR', color: color });
  };

  const onValueChange = (size) => {
    dispatch({ type: 'UPDATE_SIZE', size: size });
  };

  const onEncryptText = async (itemTipoAlgoritmoCrypto) => {
    setTypeCrypto(itemTipoAlgoritmoCrypto.tipo);
    const crypto = await Crypto(itemTipoAlgoritmoCrypto.algoritmo);
    const textoEncriptado = await crypto.encriptar(text);
    dispatch({ type: 'UPDATE_TEXT', text: textoEncriptado });
  };

  return (
    <View style={{ width: 500, height: 200 }}>
      <ColorPicker
        ref={colorPickerRef}
        onColorSelected={onColorChange}
        sliderComponent={Slider}
      />

      <NativeColorPicker
        colors={['black', 'red', 'blue']}
        selectedColor={selected}
        onSelect={onColorChange}
      />
      <Slider
        style={{ width: 200, height: 40 }}
        minimumValue={100}
        maximumValue={600}
        minimumTrackTintColor="black"
        maximumTrackTintColor="gray"
        onValueChange={onValueChange}
      />
      <TextInput value={text} onChangeText={onDataChange} />
      <Dropdown
        data={tipoAlgoritmoCripto}
        labelField={'algoritmo'}
        valueField="algoritmo"
        placeholder="Encriptar"
        value={typeCrypto}
        onChange={onEncryptText}
      />
    </View>
  );
}
