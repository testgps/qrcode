import SvgQRCode from "react-native-qrcode-svg";
import { Text } from "react-native";
import { useContextPanelQRCode } from "../providers/QRCodeProvider";

export default function QRCode() {
  const [state] = useContextPanelQRCode();

  return (
    <>
      <Text>ESCANEA CODIGO QR</Text>
      <SvgQRCode value={state.text} size={state.size} color={state.color} />
    </>
  );
}
